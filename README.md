# SpamFilter
A simple gem to detect the most common types of spam without resorting to user challenges. Check the wiki for details.