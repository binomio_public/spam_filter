require "test_helper"

class SpamFilterTest < Minitest::Test

  def setup
    SpamFilter.setup do |config|
      config.verbose = false
    end
  end

  def test_that_it_has_a_version_number
    refute_nil ::SpamFilter::VERSION
  end

  def test_that_good_messages_goes_through
    form = {
      form_generated_at: 1.minute.ago,
      hpt_city: "",
      subject: "Lipsum",
      message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut mollis erat non dolor cursus facilisis. Quisque mollis vulputate gravida. Proin eget est ut risus lobortis blandit eget in massa. Etiam aliquet mi pharetra nisl tempor eleifend. Pellentesque consectetur blandit nulla, ut elementum velit imperdiet id. Morbi eget accumsan dolor. Vivamus at lacus at eros efficitur facilisis. Maecenas non felis enim. Ut fermentum orci ut enim ullamcorper interdum. Maecenas sollicitudin suscipit lobortis. Donec pulvinar finibus ligula id mattis. Vivamus ut mollis mauris. Sed a aliquam massa, ac blandit lorem. Cras vehicula iaculis lacus non euismod."
    }
    assert_equal false, SpamFilter.spam?(form, message_fields: [ :subject, :message ])
  end

  def test_that_no_single_suspicion_is_enough_to_mark_as_spam
    forms = []
    # Suspicion: link
    forms << {
      form_generated_at: 10.minutes.ago,
      hpt_city: "",
      message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. https://google.com"
    }
    # Suspicion: honeypot
    forms << {
      form_generated_at: 35.seconds.ago,
      hpt_city: "I filled the honeypot",
      message: "Nulla rhoncus et mauris id bibendum. Curabitur justo odio, pharetra ut lacus et, dignissim malesuada tellus. "
    }
    # Suspicion: too fast
    forms << {
      form_generated_at: 1.second.ago,
      hpt_city: "",
      message: "Nullam consequat libero non mattis mattis. Phasellus tempus egestas faucibus."
    }
    # Suspicion: foreign languages
    forms << {
      form_generated_at: 7.minutes.ago,
      hpt_city: "",
      message: "Привет, мир. 你好，世界"
    }
    # Suspicion: blacklisted word
    forms << {
      form_generated_at: 2.hours.ago,
      hpt_city: "",
      message: "Lorem ipsum porn amet"
    }
    forms.each do |form|
      assert_equal false, SpamFilter.spam?(form, message_fields: [ :message ])
    end
  end

  def test_that_spam_is_marked_so
    forms = []
    # Too fast + links present
    forms << {
      form_generated_at: 1.second.ago,
      hpt_city: "",
      message: "In lacinia auctor lobortis. Sed consequat pulvinar nulla a tempor. http://example.com"
    }
    # Honeypot filled + 2 blacklisted words
    forms << {
      form_generated_at: 5.minutes.ago,
      hpt_city: "Hello world",
      message: "In lacinia auctor lobortis naked sexy."
    }
    # Foreign language + 4 blacklisted words
    forms << {
      form_generated_at: 2.minutes.ago,
      hpt_city: "Hello world",
      subject: "你好，世界",
      message: "In lacinia auctor lobortis naked porn sex sexy."
    }
    forms.each do |form|
      assert_equal true, SpamFilter.spam?(form, message_fields: [ :message ]), form
    end
  end
end
