require "spam_filter/version"
require "active_support/core_ext"

module SpamFilter
  LINK_REGEX = /https?:\/\//
  CHINESE_REGEX = /[\u4E00-\u9FFF]/ # Match Chinese characters
  RUSSIAN_REGEX = /[\u0400-\u04FF]/ # Match Russian characters

  mattr_accessor :minimum_time_to_fill, :word_blacklist, :verbose
  self.minimum_time_to_fill = 10.seconds # Penalize if time between form generation and submission is shorter than this
  self.word_blacklist = [ # Penalize when the following regexes are present
    /([^a-z]|^)porn(?=[^a-z]|$)/, # Regex looks for each word UNLESS it's part of another larger word
    /([^a-z]|^)sexy?(?=[^a-z]|$)/,
    /([^a-z]|^)naked(?=[^a-z]|$)/
  ]
  self.verbose = true # Print the message and its score to stdout

  # Method to be optionally called on server initialization to configure this gem
  def self.setup
    yield self
    raise "Expected 'minimum_time_to_fill' to be a Duration." unless self.minimum_time_to_fill.is_a? ActiveSupport::Duration
    raise "Expected 'word_blacklist' to be an array of Regexp." unless self.word_blacklist.is_a?(Array) && self.word_blacklist.all?{|w| w.is_a? Regexp }
    raise "Expected 'verbose' to be a boolean." unless self.verbose.in? [true, false]
  end

  # Returns a boolean indicating whether the message should be considered spam
  def self.spam? params, options
    return rate(params, options) > 2
  end

  # Returns a score that increases for each suspicious factor encountered
  def self.rate params, options
    message_fields = options[:message_fields] || []
    score = 0
    # Check whether the form's honeypot field was filled
    err "hpt_city paramterer is absent" if params[:hpt_city].nil?
    score += 2 if params[:hpt_city].present?
    # Check whether the form was submitted too fast for a human
    err "form_generated_at paramterer is absent" if params[:form_generated_at].nil?
    form_generated_at = Time.at(params[:form_generated_at].to_i)
    score += 2 if form_generated_at > self.minimum_time_to_fill.ago
    # Check whether the message fields have links
    message_fields.each do |field|
      if params[field] =~ LINK_REGEX
        score += 1
        break
      end
    end
    # Check whether the message fields have Chinese or Russian characters
    message_fields.each do |field|
      if params[field] =~ CHINESE_REGEX || params[field] =~ RUSSIAN_REGEX
        score += 1
        break
      end
    end
    # Check presence of blacklisted words
    word_count = 0
    message_fields.each do |field|
      text = params[field].downcase
      self.word_blacklist.each do |word_regex|
        word_count += text.scan(word_regex).count
      end
    end
    score += (word_count / 2).floor
    if self.verbose
      puts "Running spam filter on message:"
      puts params.inspect
      puts "SCORE: #{score}"
    end
    return score
  end

  def self.err message
    raise SpamFilter::Error.new message
  end

  class Error < StandardError; end
end
